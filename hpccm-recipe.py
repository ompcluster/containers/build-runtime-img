#!/usr/bin/env python

from __future__ import print_function
from functools import partial

import re
import argparse
import hpccm
import os

import hpccm.config

from hpccm.building_blocks import *
from hpccm.primitives import *

import itertools


def generateRecipe(baseImage, wd=hpccm.config.g_wd):
    Stage0 = hpccm.Stage()

    Stage0 += baseimage(image=baseImage)

    Stage0 += comment('LLVM installation')
    Stage0 += shell(commands=[f'mkdir {wd}/ompc'])
    Stage0 += copy(src='./install/install-llvm.sh', dest=f'{wd}/ompc',
                   _post=True)
    Stage0 += shell(commands=[
        f'{wd}/ompc/install-llvm.sh {wd}/ompc '
        + os.getenv('LLVM_BRANCH', 'main'),
        f'rm -rf {wd}/ompc/'
        ])

    Stage0 += environment(variables={'LD_LIBRARY_PATH': '/usr/local/lib:$LD_LIBRARY_PATH',
                                     'LIBRARY_PATH': '/usr/local/lib:$LIBRARY_PATH',
                                     'C_PATH': '/usr/local/lib:$LIBRARY_PATH'})

    Stage0 += comment('OmpTracing installation')
    Stage0 += packages(apt=['libjson-c-dev'], yum=['json-c-devel'], epel=True)
    Stage0 += generic_cmake(prefix='/opt/omptracing',
                            repository='https://gitlab.com/ompcluster/omptracing.git',
                            preconfigure=['export CC=clang', 'export CXX=clang++'])

    if '20.04' in baseImage:
        Stage0 += comment('OMPC Bench installation')
        Stage0 += generic_build(build=['pip3 install -e .'],
                                repository='https://gitlab.com/ompcluster/ompcbench.git')

    Stage0 += comment('TaskBench installation')
    Stage0 += generic_build(build=['export CXX=clang++',
                                   'DEFAULT_FEATURES=0 USE_OMPCLUSTER=1 USE_OPENMP=1 ./get_deps.sh',
                                   './build_all.sh'],
                            install=['mkdir -p /opt/task-bench && cp -R ./* /opt/task-bench'],
                            repository='https://gitlab.com/ompcluster/task-bench.git',
                            branch='fix/openmp-driver')
    Stage0 += generic_build(build=['export CXX=mpicxx',
                                   'DEFAULT_FEATURES=0 TASKBENCH_USE_MPI=1 USE_MPI_OPENMP=1 ./get_deps.sh',
                                   './build_all.sh'],
                            install=['mkdir -p /opt/task-bench-mpi && cp -R ./* /opt/task-bench-mpi'],
                            repository='https://gitlab.com/ompcluster/task-bench.git',
                            branch='fix/openmp-driver')

    Stage0 += comment('Install tests and utilities')
    # Add tests folder to containers
    Stage0 += copy(src='./tests/', dest='/opt/tests/', _post=True)
    # Copy utilities
    Stage0 += shell(commands=['mkdir /opt/ompc'])
    Stage0 += copy(src='./utils/', dest='/opt/ompc/', _post=True)

    hpccm.config.set_container_format('docker')

    return Stage0


def combine(list1, list2):
    return map(lambda x: '-'.join(x), list(itertools.product(list1, list2)))


def main():
    print("Generating Runtime image definitions...")

    # list of building blocks combinations to use
    allConf = ["ubuntu20.04"]
    allConf += combine(allConf, ["cuda11.2"])
    allConf = combine(allConf, ["mpich", "mpich-legacy", "mpich34",
                                "openmpi", "openmpi-legacy",
                                "mvapich2"])
    listConf = [*allConf]
    allConf = combine(listConf, ["ofed5"])
    listConf += [*allConf]

    # print(*listConf)
    outputFolder = "Dockerfiles"

    for currConf in listConf:
        baseImage = "ompcluster/hpcbase:" + currConf
        defFileText = str(generateRecipe(baseImage))
        defFileName = currConf

        # save container definition file
        with open(f"{outputFolder}/{defFileName}", "w") as text_file:
                text_file.write(str(defFileText))


if __name__ == "__main__":
    main()
