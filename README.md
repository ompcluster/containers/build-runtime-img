# How to Build Feature Branch

This repository's pipeline allows the manual triggering of feature branches. For detailed instructions, please refer to the [wiki](https://gitlab.com/ompcluster/ompcluster-wiki/-/wikis/Generating-feature-branch-Docker-Hub-images).


# Building Runtime Images

This repository is used to build Runtime container images. Those *runtime*
images contains the Clang compiler and LLVM runtimes necessary to run OpenMP
task-based programs on HPC clusters.

The containers images are built automatically by CI/CD and are available on
the following Docker Hub repository:
https://hub.docker.com/r/ompcluster/runtime

## Local Manual Build

First, you need to install
[HPC Container Maker (HPCCM)](https://github.com/NVIDIA/hpc-container-maker),
an open-source tool to generates container specifications files (like
Dockerfiles). An HPCCM script is a Python recipe) that can generate multiple
container specifications.

HPCCM can be installed from  Pypi or Conda using the following commands:
```
sudo pip install hpccm
```
or
```
conda install -c conda-forge hpccm
```

Then, just run the following command to generate the definitions:
```
python3 hpccm-recipe.py
```

The script provided in this repository generates both Docker and Singularity
definitions. However, the Docker images performs some extra configurations that
makes it compatible with our Deploy-Swarm script: the password-less SSH
connection is configured inside the container, while Singularity can be run
using the host configuration.

Finally, any of the generated images can be built using Docker and Singularity.
