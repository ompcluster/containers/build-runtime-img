#include <stdio.h>
#include <stdlib.h>

//#include <omp.h>

#define N 1000

#define SPARSE 0

void init(float *a, float *b) {
  int i, j;
  for (i = 0; i < N; ++i) {
    for (j = 0; j < N; ++j) {
      if ((i != j || (i%2==0)) && SPARSE) {
        a[i * N + j] = 0;
        b[i * N + j] = 0;
      } else {
        a[i * N + j] = ((float)i * j) / N;
        b[i * N + j] = ((float)i * (j + 1)) / N;
      }
    }
  }
}

int compareResults(float *c1, float *c2) {
  int i, j, fail;
  fail = 0;

  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      if (c1[i * N + j] != c2[i * N + j]) {
        fail++;
        if (i < 10)
          fprintf(stdout, "%f != %f \n", c1[i * N + j],
                  c2[i * N + j]);
      }
    }
  }

  // Print results
  printf("Non-Matching Outputs: %d\n", fail);
  return fail;
}

void MatMul(float *a, float *b, float *c) {
  #pragma omp target map(to: a[:N*N], b[:N*N]) map(from: c[:N*N])
  #pragma omp parallel for
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      c[i * N + j] = 0;
      for (int k = 0; k < N; ++k) {
        c[i * N + j] += a[i * N + k] * b[k * N + j];
      }
    }
  }

}

void MatMul2(float *a, float *b, float *c) {
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      c[i * N + j] = 0;
      for (int k = 0; k < N; ++k) {
        c[i * N + j] += a[i * N + k] * b[k * N + j];
      }
    }
  }

}

int main() {

//  int N = 1000;
  float *a = (float*) malloc(sizeof(float)*N*N);
  float *b = (float*) malloc(sizeof(float)*N*N);
  float *c = (float*) malloc(sizeof(float)*N*N);
  float *c2 = (float*) malloc(sizeof(float)*N*N);

  printf("Initialize Matrices\n");
  init(a, b);

  printf("Compute MatMul locally\n");
  MatMul2(a, b, c2);

  printf("Compute MatMul remotely\n");
  MatMul(a, b, c);

  printf("Compare results\n");
  compareResults(c, c2);

  return 0;

}
