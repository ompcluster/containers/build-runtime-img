#!/bin/bash
set -ex

echo "Installing LLVM/Clang..."

if [[ $# -ne 2 ]]; then
  echo "Error: wrong number of arguments"
  echo "install-llvm.sh <WORK_DIR> <LLVM_BRANCH>"
  exit 1
fi

WORK_DIR=$1
LLVM_BRANCH=$2

# Read CUDA version
if [ -f /usr/local/cuda/version.txt ]; then
  CUDA_VERSION=$(($(cat /usr/local/cuda/version.txt | sed 's/.*Version \([0-9]\+\)\.\([0-9]\+\).*/\1\2/')+0))
fi

git clone --depth 1 -b $LLVM_BRANCH https://gitlab.com/ompcluster/llvm-project.git $WORK_DIR/llvm-project

ln -s /usr/bin/ninja /usr/bin/ninja-build || true # use ninja or ninja-build interchangeably
ln -s /usr/bin/ninja-build /usr/bin/ninja || true

# Build LLVM projects with the previous built clang
mkdir -p $WORK_DIR/build-llvm
cd $WORK_DIR/build-llvm

if [ -z "$CUDA_VERSION" ]; then
  # Do not build NVPTX target
  cmake $WORK_DIR/llvm-project/llvm \
        -GNinja -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_C_COMPILER=clang-12 \
        -DCMAKE_CXX_COMPILER=clang++-12 \
        -DCMAKE_INSTALL_PREFIX="/usr/local" \
        -DLLVM_OCAML_INSTALL_PATH="/usr/local" \
        -DLLVM_USE_LINKER=gold \
        -DLLVM_ENABLE_PROJECTS="clang" \
        -DLLVM_ENABLE_RUNTIMES="libcxx;libcxxabi;compiler-rt;openmp" \
        -DLLVM_TARGETS_TO_BUILD="X86" \
        -DCLANG_VENDOR="OmpCluster" \
        -DLIBOMPTARGET_ENABLE_DEBUG=1
else
  if [ $CUDA_VERSION -ge 100 ]; then
    SUPPORTED_CC="35,37,50,52,53,60,61,70,72,75"
  elif [ $CUDA_VERSION -ge 90 ]; then
    SUPPORTED_CC="35,37,50,52,53,60,61,70,72"
  else
    SUPPORTED_CC="35,37,50,52,53,60,61"
  fi

  cmake $WORK_DIR/llvm-project/llvm \
        -GNinja -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_C_COMPILER=clang-12 \
        -DCMAKE_CXX_COMPILER=clang++-12 \
        -DCMAKE_INSTALL_PREFIX="/usr/local" \
        -DLLVM_OCAML_INSTALL_PATH="/usr/local" \
        -DLLVM_USE_LINKER=gold \
        -DLLVM_ENABLE_PROJECTS="clang" \
        -DLLVM_ENABLE_RUNTIMES="libcxx;libcxxabi;compiler-rt;openmp" \
        -DLLVM_TARGETS_TO_BUILD="X86;NVPTX" \
        -DCLANG_VENDOR="OmpCluster" \
        -DCLANG_OPENMP_NVPTX_DEFAULT_ARCH=sm_35 \
        -DLIBOMPTARGET_NVPTX_COMPUTE_CAPABILITIES=$SUPPORTED_CC \
        -DLIBOMPTARGET_NVPTX_BC_LINKER=llvm-link-12 \
        -DLIBOMPTARGET_ENABLE_DEBUG=1
fi

cmake --build .
cmake --build . --target install

# Remove temporary files
rm -rf $WORK_DIR/build-clang $WORK_DIR/build-llvm
