#!/usr/bin/python3
import json
import glob

res = {
  "traceEvents" : []
}

for filename in glob.glob("*.json"):
  with open(filename, 'r') as file:
    data = json.load(file)
    if "traceEvents" in data:
      res["traceEvents"].extend(data["traceEvents"])
    else:
      res["traceEvents"].extend(data)

with open("trace.json", 'w') as file:
  file.write(json.dumps(res))
